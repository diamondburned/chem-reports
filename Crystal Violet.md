# Crystal Violet Lab

## Prelab

| $\frac{ml \ stock}{ml \ water}$ | %transmittance | Molar conc ($\mu m$) |
|---------------------------------|----------------|----------------------|
| 10/0                            | 37.7%          | 25.0                 |
| 6/4                             | 53.4%          | 15.0                 |
| 4/6                             | 70.6%          | 10.0                 |
| 3/7                             | 78.4%          | 7.5                  |
| 2/8                             | 82.4%          | 5.0                  |
| 1/9                             | 93.2%          | 2.5                  |
| 0/10                            | 100%           | 0.0                  |
|                                 |                |                      |

![](https://gitlab.com/diamondburned/chem-reports/raw/master/assets/molarconc%20vs%20%25trans.png)

## Investigation

### $4 \ ml \ NaOH$ : $4 \ ml \ CV$

| Second | %transm | Transm | Molar conc | ln(conc) | 1/conc |
|--------|---------|--------|------------|----------|--------|
| 0      | 68.0%   | 0.68   | 11.5       | 2.44     | 0.09   |
| 20     | 74.8%   | 0.75   | 8.82       | 2.18     | 0.11   |
| 40     | 80.0%   | 0.80   | 6.78       | 1.91     | 0.15   |
| 60     | 82.6%   | 0.83   | 5.76       | 1.75     | 0.17   |
| 80     | 86.0%   | 0.86   | 4.43       | 1.49     | 0.23   |
| 100    | 87.8%   | 0.88   | 3.73       | 1.31     | 0.27   |
| 120    | 89.0%   | 0.89   | 3.25       | 1.18     | 0.31   |
| 140    | 90.0%   | 0.90   | 2.86       | 1.05     | 0.35   |
| 160    | 90.4%   | 0.90   | 2.71       | 1.00     | 0.36   |
| 180    | 90.6%   | 0.91   | 2.63       | 0.97     | 0.38   |
| 200    | 91.0%   | 0.91   | 2.47       | 0.90     | 0.40   |
| 220    | 91.2%   | 0.91   | 2.39       | 0.87     | 0.41   |

![](https://gitlab.com/diamondburned/chem-reports/raw/master/assets/Standard%20graph_%20Molarity%20of%20CV%20vs%20%25Transmittance.png?inline=false)
![](https://gitlab.com/diamondburned/chem-reports/raw/master/assets/molar%20conc%20vs%20Time.png?inline=false)
![](https://gitlab.com/diamondburned/chem-reports/raw/master/assets/ln%28molar%20conc%29%20vs%20Time.png?inline=false)
![](https://gitlab.com/diamondburned/chem-reports/raw/master/assets/1_%28molar%20conc%29%20vs%20Time.png?inline=false)

=> **Second rate**

## Conclusion

The purpose of this lab is to identify the rate law for the reaction of crystal violet and sodium hydroxide, along with identifying the rate order. For the prelab, the data I've got was shown above, in the Prelab section. For the investigation, the data was shown in the Investigation section. From the table, I made two graphs. From these graphs, comparing between 1/molarity and ln(molarity), I think the reaction is second rate, as the 1/molarity graph looks straight comparing to ln(molarity).


