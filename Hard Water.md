# Hard Water Lab

## Data

### $CaCl_2$

Watch glass mass: $102.889g$
Filter paper mass: $0.682g$
Total mass after drying: $109.94g$

=> Mass of solid: $109.94 - 0.682 - 102.889 = 6.369g$

### A-F samples

$5ml$ unknown were put into $2g$ of solid $Na_2CO_3$ dissolved in water.

| Sample   | Filter Paper | Filter Paper + Solid | Solid | $CaCl_2$ |
|----------|--------------|----------------------|-------|----------|
| A        | 0.649        | 0.985                | 0.336 | 0.404    |
| B        | 0.697        | 0.954                | 0.257 | 0.309    |
| C        | 0.658        | 0.760                | 0.102 | 0.123    |
| D        | 0.694        | 1.046                | 0.352 | 0.423    |
| E        | 0.694        | 1.203                | 0.509 | 0.612    |
| F        | 0.665        | 1.288                | 0.623 | 0.749    |
| Original | 0.653        | 2.316                | 1.663 | 2.000    |

![](https://gitlab.com/diamondburned/chem-reports/raw/master/assets/hard%20water.png?inline=false)

#### Order: C - B - A - D - E - F 

## Conclusion

The purpose of the lab is to rank six water samples in order of increasing hardness through ion precipitation and separation by filtering. The equation is $Na_2 + CaCl_2 \rightarrow CaCO_3 + NaCl$. The $CaCl_2$ masses were calculated by using the original data, which yielded $1.663g$ for $2g$ of $CaCl_2$ as the base ratio, then use that on the solid masses collected after drying. The determined order is C - B - A - D - E - F. I think an error during the lab could possibly be not drying multiple times the sample, or minor fluctuations on the amount of $CaCl_2$ mixed.
