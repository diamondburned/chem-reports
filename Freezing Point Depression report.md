
# Freezing Point Depression report

## Pre-Lab questions

1. What is a colligative property?
	- Properties that depend on the ratio of solute particles to solvent particles
2. Is isopropyl alcohol ($C_3H_7OH$) a strong, weak or non-electrolyte? How do you know?
	- Non-electrolyte, because it has no ions
3. What is the accepted value for the molar mass of isopropyl alcohol?
	- $60.1$g/mol
4. Based on your answer to question #2, what is the "i" value for isopropyl alcohol?
	- 1
5. Why is it important to experimentally find the freezing point of pure water rather than just assigning it a value of $0.0 ^{\circ}$C?
	- Pressure might be a factor that affects the water freezing point in the same condition with the alcohol
6. A solution is prepared by mixing 2.17 grams of an unknown non-electrolyte with 225.0 grams of chloroform. The normal freezing point of chloroform is $-63.5 ^{\circ}$C. The freezing point of the solution is experimentally determined to be $-65.0 ^{\circ}$C. The $K_t$ value for chloroform is $4.68^{\circ}C/m$. Find the molar mass of the unknown.
	- $\Delta T = -65.0+63.5 = -1.5^{\circ}$C
	- $-1.5 = (-4.68)(m)(1)$ => $m = 0.32$
	- $0.32 = \frac{n}{0.225g}$ => $n = 0.072$
	- $0.072 = \frac{2.17}{M}$ => $M = 30.14g$
7. If the unknown solute in #6 was methanol ($CH_3OH$), calculate the percent error for the experiment.
	- Molar mass of Methanol: $32.04g$
	- Experimental molar mass: $30.14g$
	- $\%error = |\frac{30.14-32.04}{32.04}|=5.93\%$

## Data

| Time (minutes) | Water Temperature | Alcohol Temperature |
| :------------- | :---------------: | :-----------------: |
| 0 (Initial)    | $19 ^{\circ}$C    | $19 ^{\circ}$C      |
| 0.5            | $16 ^{\circ}$C    | $14 ^{\circ}$C      |
| 1.0            | $12 ^{\circ}$C    | $6  ^{\circ}$C      |
| 1.5            | $9  ^{\circ}$C    | $1  ^{\circ}$C      |
| 2.0            | $6  ^{\circ}$C    | $-2 ^{\circ}$C      |
| 2.5            | $3  ^{\circ}$C    | $-5 ^{\circ}$C      |
| 3.0            | $1  ^{\circ}$C    | $-7 ^{\circ}$C      |
| 3.5            | $-1 ^{\circ}$C    | $-9 ^{\circ}$C      |
| 4.0            | $-2 ^{\circ}$C    | $-10^{\circ}$C      |
| 4.5            | $-2 ^{\circ}$C    | $-11^{\circ}$C      |
| 5.0            | $-2 ^{\circ}$C    | $-12^{\circ}$C      |
| 5.5            | $-2 ^{\circ}$C    | $-13^{\circ}$C      |
| 6.0            | $-2 ^{\circ}$C    | $-13^{\circ}$C      |
|                |                   |                     |

| |  |
|:--|---:|
| $V_i$ | 5ml |
| $V_f$ | 10ml |
| $V_{alcohol}$ | 5ml |
| |  |

## Analysis

- Freezing point of water: $-2^{\circ}$C
- Freezing point of alcohol: $-13^{\circ}$C
- $\Delta T = -11^{\circ}$C
- $-11^{\circ} C = -1.86^{\circ}C/m * m * 1$ 
	- Molality: $5.9m$
- Grams of solute added: $0.785 \ g/ml * 5 = 3.925 \ g$
- $5.9m = \frac{n_{solute}}{0.010l \ water}$ 
	- $n_{solute} = 0.059 \ mol$
- $0.059 \ mol = \frac{3.925g}{M}$  
	- Experimental molar mass: $M=66.53 \ g/mol$

### Error

| Experimental | Theoretical |
| - | - |
| $66.53$ | $60.10$ |

1. $\%error = \frac{66.53-60.10}{60.10}=10.70\%$
2. The experimental molar mass would've been lower if some of the alcohol evaporated.
3. It would've caused the molar mass to be low, as the alcohol would've evaporate during the experiment.

## Conclusion

The purpose of this is to experimentally determine the molar mass is isopropyl alcohol using Freezing Point depression. From the table, I have concluded that the freezing point of water and alcohol respectively is $-2^{\circ}$C and $-13^{\circ}$C. The freezing depression is calculated to be $-11^{\circ}$C. The molality calculated is $5.9m$, which results in $0.059mol$. Calculating that with the grams of solute calculated ($3.925g$) gives $66.53g/mol$. This is a $10.70\%$ error. I suspect the error is caused by putting too much alcohol in, or too little water in.
