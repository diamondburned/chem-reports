# Calcium Lab

## Part one

Equation: $ Ca + 2HNO_3 \rightarrow Ca(NO_3)_2 + H_2 $

Theoretical mass for $Ca$: $0.750g$

Experimental mass for $Ca$: $0.755g$

| 30th second   | Temperature        |
| ------------- | ------------------ |
| $0$ (initial) | $20 ^\circ\rm C$   |
| $1$           | $22 ^\circ\rm C$   |
| $2$           | $25 ^\circ\rm C$   |
| $3$           | $27 ^\circ\rm C$   |
| $4$           | $30.5 ^\circ\rm C$ |
| $5$           | $30 ^\circ\rm C$   |
| $6$           | $29.5 ^\circ\rm C$ |
| $7$           | $31 ^\circ\rm C$   |
| $8$           | $31.5 ^\circ\rm C$ |
| $9$           | $32 ^\circ\rm C$   |
| $10$          | $31 ^\circ\rm C$   |
| $11$          | $31 ^\circ\rm C$   |
| $12$          | $30.5 ^\circ\rm C$ |
| $13$          | $30 ^\circ\rm C$   |

### Theoretical

$$
\begin{align}
c &= ( Ca(NO_3)_2 + H_2 ) - ( Ca + 2HNO_3 ) 
\\
&= (-956.1+0) - (0+413.14) 
\\
&= 542.96 \frac{kJ}{mol}
\end{align}
$$

### Experimental

$$
\begin{align}
n_{Ca} &= \frac{0.755g}{40.08g/mol} 
\\
&= 0.0188mol
\\
\\
q&=ms \Delta t 
\\
&=(100+0.75)(4.187)(12)
\\
&=5058.5J
\\
&=5.06kJ
\\
\\
c_n&= \frac{5.06kJ}{0.0188mol} 
\\ 
&= 269 \frac{kJ}{mol}
\end{align}
$$

### %error

$ \%_{Error} = |\frac{269 - 542.96}{542.96}| \times 100\% = 50.5\% $

## Part two

Equation: $ HNO_3 + NaOH \rightarrow NaNO_3 + H_2O $

$$
\begin{align}
V_{NaOH} &= 16ml = 0.016l
\\
\\
n_{NaOH} &= n_{HNO_3}
\\
&=0.100M \times 0.016l 
\\
&= 0.0016mol
\end{align}
$$

From part 1: $n_{HNO_3} = 0.50M \times 0.1l = 0.05mol$

=> Excess: $0.05mol - 0.016mol = 0.0484mol$

=> $n_{Ca} = \frac{0.0484mol}{2} = 0.0242mol $

=> $M_{Ca} = \frac{0.75g}{0.0242mol} = 31.0 \frac{g}{mol}$

### %error

$\%_{error} =  |\frac{31.0 - 40.08}{40.08}| \times 100\% = 22.7\% $

## Conclusion

​	The purpose of this lab is, in short, to calculate the molar mass of Calcium ($Ca$) from the reaction of solid Calcium metal reacting with nitric acid ($HNO_3$) followed by titration with sodium hydroxide ($NaOH$). 

​	For the heat data of part 1, the initial temperature was $20^\circ\rm C$, peaking at $32^\circ\rm C$ then goes down to $30^\circ\rm C$. The percentage error calculated was 50.5%. I suspect the high percentage error has to do with the temperature not matching the theoretical data obtained, also the $0.005g$ difference for the mass of $Ca$. 

​	For part 2, we got $16ml$ of $NaOH$ to titrate the acid solution. Calculations showed $31.0g/mol$ for molar mass of $Ca$, while theoretically it is $40.08g/mol$. This yielded a $22.7%$ error. I suspect the error has to do with adding too much $NaOH$, leading to wrong results.