# Titration Lab

## Part one

Equation: 
$$
KHC_8H_4O_4 + NaOH \rightarrow H_2O + NaKC_8H_4O_4 
$$

Mole of KHP: 
$$
\frac{0.50g \ KHP}{204.22g/mol \ KHP} = 0.0024 mol \ KHP 
$$

Molarity of KHP: 
$$
\frac{0.0024 mol \  KHP}{0.06l \ KHP \ solution} = 0.04M \ KHP
$$

#### First attempt

$$
V_{1_{NaOH}} = 16.9ml = 0.0169l
$$

$$
M_{NaOH} = \frac{(0.04)(0.06)}{0.0169} = 0.14M
$$

#### Second attempt

$$
V_{2_{NaOH}} = 14.6ml = 0.0146l
$$

$$
M_{NaOH} = \frac{(0.04)(0.06)}{0.0146} = 0.16M
$$

#### Average

$$
\overline{M_{NaOH}} =  \frac{0.14+0.16}{2} = 0.15M 
$$

##### Theoretical molarity

$$
M_{NaOH} = 0.150M
$$

#### Percentage error 

$$
\%_{error} = |\frac{0.150-0.15}{0.150}| \times 100\% = 0\%
$$



## Part two

#### Table for trials of $ NaOH$ needed for titration of $ HCl $:

| $ V \ 0.100M \ HCl $ | Trial 1    | Trial 2    | Trial 3    | Average    |
| -------------------- | ---------- | ---------- | ---------- | ---------- |
| $ 10.0ml = 0.01l$    | $ 6.8ml $  | $ 6.9ml $  | $ 6.9ml $  | $ 6.9ml $  |
| $ 30.0ml = 0.03l$    | $ 20.8ml $ | $ 21.2ml $ | $ 19.9ml $ | $ 20.6ml $ |

#### Table for molarity of $ NaOH $ and $ HCl$

| $ V_t \ NaOH $ (average) | M_{NaOH} | $n_{NaOH} = n_{HCl}$ | $V_{HCl}$ | $ M_{HCl} $ |
| ------------------------ | -------- | -------------------- | --------- | ----------- |
| $6.9ml = 0.0069l$        | $0.15M$  | $0.001035 \ mol$     | $0.01l$   | $0.1035M$   |
| $20.6ml = 0.0206l$       | $0.15M$  | $0.00309 \ mol$      | $0.03l$   | $0.103M$    |

#### Average $M_{HCl}$

$$
\overline{M_{HCl}} = \frac{0.1035+0.103}{2} = 0.10325M
$$

#### Percentage error

$$
\%_{error} = |\frac{0.10325-0.100}{0.100}| \times 100\% = 3.25\%
$$



## Unknown

Solution: $ O $

Experimental molarity for $ NaOH$ was $0.145\ mol$
$$
V_{sample \ of \ HCl} = 10ml = 0.010l
$$

$$
M_{O} = \frac{(M_{NaOH})(V_t)}{V_{sample \ of \ HCl}}
$$

#### First attempt

$$
V_1 = 7.3ml = 0.0073l 
$$

$$
M_O = \frac{(0.145)(0.0073)}{0.010} = 0.10585 = 0.106M 
$$

#### Second attempt

$$
V_2 = 7.2ml = 0.0072l 
$$

$$
M_O = \frac{(0.145)(0.0072)}{0.010} = 0.1044 = 0.104M 
$$

#### Average

$$
\overline{M_O} = \frac{0.106+0.104}{2} = 0.105M
$$

#### Theoretical molarity

$$
M_O = 0.110M 
$$

#### Percentage error

$$
\%_{error} = |\frac{0.105-0.110}{0.110}| \times 100\% = 4.54\%
$$

*A grade of 96% or 48/50 was given for this.*



## Conclusion

​	The purpose of this lab was to create and verify the molarity or $ NaOH $ using $ KHP $; to verify the molarity of a known $ HCl $ sample and to verify the titration technique by calculating the molarity of a sample of unknown $HCl​$ molarity.

​	The equation for part 1 is:
$$
KHP + NaOH \rightarrow H_2O + NaKP
$$

​	The equation for part 2 and 3 is:
$$
HCl + NaOH \rightarrow NaCl + H_2O
$$
​	For part 1, the titration data was $16.9ml$ and $14.6ml$ for the volume of $ NaOH $, for a total of 2 attempts from these data and the $0.04M \ KHP$ solution that we made. We got $0.14M \ NaOH$ for the first attempt and $ 0.16M \ NaOH $ for the second attempt. The average molarity was $ 0.15M $. Based on the theoretical molarity of $0.15M$, we got $0\%$ error. 

​	For part 2, we had an average of $6.9ml \ NaOH$ for $10ml \ 0.1M \ HCl$. For $40ml \ 0.1M \ HCl$, the data was $20.6ml \ NaOH$. We multiplied that by the molarity of $NaOH$ for the molar of $NaOH$ which is also for $HCl$. We divided that by the volume of $HCl$ aliquot for the (experimental) molarity of $HCl$. The averaged result is $0.10325M $, which returned a $3.25\%$ error.

​	For part 3, I did a few attempts and picked the best 2 data for the unknown. I used a $0.145M$ solution of $NaOH$ and a $10ml$ or $0.01l$ sample of unknown. The two attempts gave $7.3ml$ and $7.2ml$, which, when divided by the molarity of $NaOH$ over the volume of unknown sample, gave back $0.106M$ and $0.105M \ HCl$, respectively. The average molarity was $0.105M$ while the actual molarity was $0.110M$. Based on that, the percentage error was $4.54\%$.