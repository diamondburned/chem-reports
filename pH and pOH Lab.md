# pH Lab

## Equation

$CH_3COOH + NaOH \rightleftharpoons CH_3COONa + H_2O$

## Data

### Theoretical

pH = $7$ => $K_a$ = $10^{-7}$

### Experimental

Amount of $NaOH$: $1.8\ ml$
$pH$: $6.91\ pH$
=> $K_a$ = $10^{-6.91}$ = $1.23 \times 10^{-7}$

### Error

$\frac{|1.23 \times 10^{-7} - 10^{-7}|}{10^{-7}} \times 100\% = 23\%$

# pOH Lab

	

## Conclusion

The purpose of this lab was to calculate the $K_a$ of the reaction between $CH_3COOH$ and $NaOH$.  The empirical formula is $CH_3COOH + NaOH \rightleftharpoons CH_3COONa + H_2O$. The percentage error is $23\%$. To titrate, we used  
