## Unknown

Solution: $ O $

$  V_{sample \ of \ HCl} = 10ml = 0.010l  $

$ M_{NaOH} = 0.145 mol $ (different NaOH used)

$ M_{O} = \frac{(M_{NaOH})(V_t)}{V_{sample \ of \ HCl}} $

#### First attempt

$ V_1 = 7.3ml = 0.0073l $

$ M_O = \frac{(0.145)(0.0073)}{0.010} = 0.10585 = 0.106M $

#### Second attempt

$ V_2 = 7.2ml = 0.0072l $

$ M_O = \frac{(0.145)(0.0072)}{0.010} = 0.1044 = 0.104M $

#### Average

$ \overline{M_O} = \frac{0.106+0.104}{2} = 0.105M $

