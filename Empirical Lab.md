# Empirical Lab

## First part

**Formula**  $ CuSO_4 \cdot 5H_2O <-> 5H_2O + CuSO_4 $

| Mass of Crucible | Mass of Crucible and CuSO~4~ · 5H~2~O | Mass of CuSO~4~ · 5H~2~O |
| :--------------: | :-----------------------------------: | :----------------------: |
|     $5.579g$     |               $7.502g$                |   $7.502-5.579=1.923g$   |

|                      | Trial 1  | Trial 2  | Trial 3  |
| :------------------- | :------- | :------- | :------- |
| Pre-cook mass ($g$)  | $7.502g$ | $7.144g$ | $7.086g$ |
| Post-cook mass ($g$) | $7.144g$ | $7.086g$ | $6.874g$ |
| Mass of $H_2O$ ($g$) | $0.358g$ | $0.058g$ | $0.207g$ |

$ M_{CuSO4 \cdot 5H_2O} = 249.70\frac{g}{mol} $

$ n_{CuSO_4 \cdot 5H_2O} = \frac{m_{CuSO4 \cdot H_2O}}{M_{CuSO4 \cdot H_2O}} = \frac{1.923}{249.7} = 0.007701 mol$

$ n_{H_2O} = n_{CuSO4 \cdot H_2O} \times 5 = 0.007701 \times 5 = 0.03851mol $

 $ m_{H_2O} = n_{H_2O} \times M_{H_2O} = 0.03851 \times 18.016 = 0.6938g $

$ \%_{Theoretical \ composition \ of \ H_2O} = \frac{m_{H_2O}}{m_{CuSO4 \cdot H_2O}} \times 100\% = \frac{0.6938}{1.923} \times 100\% = 36.08\% \ H_2O $

$ \%_{Actual \ composition \ of \ H_2O} = \frac{m_{H_2O}}{m_{CuSO4 \cdot H_2O}} \times 100\% = \frac{0.358+0.058+0.207}{1.923} = 32.40\% \ H_2O $

$ \%_{Error} = |\frac{32.40 - 36.08}{36.08}| \times 100\% = 10.20\% $

## Second part

**Formula ** $ 2Mg + O_2 \rightarrow 2MgO $

| Mass of Crucible | Mass of Crucible and Mg | Mass of Mg |
| ---------------- | ----------------------- | ---------- |
| $4.203g$         | $4.337g$                | $0.134$g   |

|         | Trial 1 | Trial 2 | Trial 3 |
| ------- | ------- | ------- | ------- |
| Pre-cook mass ($g$)  | $4.337g$ | | |
| Post-cook mass ($g$) | $4.352g$ | | |
| Mass of $MgO$ ($g$) | $0.015g$ | | |

$ n_{MgO} = \frac{m_{MgO}}{M_{MgO}} = \frac{0.015}{40.3} = 0.0003722mol $

$ n_{O_2} = \frac{n_{Mg}}{2} = \frac{0.0003722}{2} = 0.0001861g $

$ m_{O_2} = n_{O_2} \times M_{O_2} = 0.0001861 \times 32 = 0.0060g $

##### Mass of each element in MgO

$ m_O = \frac{m_{O_2}}{2} = \frac{0.0060}{2} = 0.0030g $

$ m_{Mg} = m_{MgO} - m{O} = 0.015 - 0.0030 = 0.012g $

**Actual yield**  0.0030g of Oxygen and 0.012g of Magnesium

$ n_{O_2} = \frac{m_{O_2}}{M_{O_2}} = \frac{0.0030}{16} = 0.00019mol $

$ n_{Mg} = \frac{m_{Mg}}{M_{Mg}} = \frac{0.012}{24.30} = 0.00049mol $

$ n_{O_2} < n_{Mg} $ => $  \left[ \begin{array} \ \frac{0.00019}{0.00019} = 1 \\ \frac{0.00049}{0.00019} = 2.5 \end{array} \right. $ => $  \left[ \begin{array} \ x = 1 \times 2 = 2 \\ y = 2.5 \times 2 = 5 \end{array} \right. $

**Formula for Mg~x~O~y~**  $ Mg_xO_y = Mg_2O_5 $

$ \%_{Error} = \frac{0.015-0.222}{0.222} \times 100\% = 93\% $

## Conclusion

​	The purpose of the lab was to find the theoretical and actual percent composition of water in Copper II Sulfate Pentahydrate and the theoretical yield, actual yield, empirical formula and percent error for $ Mg_xO_y $. The equation is $ CuSO_4 \cdot 5H_2O \leftrightarrow 5H_2O + CuSO_4 $ for Part 1. For Part 2, the equation is $ 2Mg + O_2 \leftrightarrow 2MgO $. The experimental percent composition of water in $ CuSO_4 \cdot 5H_2O $ is 32.40% and the percent error was 10.20%. The empirical formula of $ MgO $ is $ Mg_5O_2 $ and the percent error was 93%. The theoretical yield was 0.222g of $ MgO $ and we got 0.015g of $ MgO $. To obtain the percent composition of $ H_2O $, we subtracted the post-cook mass from the pre-cook mass to get the mass of water lost, then divided that with the mass of Copper II Sulfate Pentahydrate to get 32.40%. The percent error was 10.20%. To obtain the theoretical mass of $ MgO $, we subtracted the mass of the crucible with magnesium and the mass of only the crucible to get 0.134g, then used stoichiometry to get the mass of $ MgO $. From our data, we obtained our experimental yield of $ MgO ​$ to get the grams of Oxygen and Magnesium to find the empirical formula and the percent error. 