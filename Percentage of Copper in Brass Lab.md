# Percentage of Copper in Brass Lab

| Concentration | Absorbance |
| ------------- | ---------- |
| $0.400M$      | $0.546$    |
| $0.200M$      | $0.314$    |
| $0.100M$      | $0.159$    |
| $0.0500M$     | $0.089$    |
| $0.0250M$     | $0.071$    |
| Unknown       | $0.256$    |

![](/home/diamond/Pictures/graph.png)

Equation for Line of Best fit: $y = 1.30x + 0.0348$

##### Concentration of Unknown

$$
\begin{align}
0.256 = 1.30&x + 0.0348
\\
&x = 0.170
\end{align}
$$

=> $0.170M$

## Conclusion

The purposes of this lab were to identify the relationships between color, wavelength, absorbances and concentrations and to find the mass percent of copper in a brass sample. The data for $0.4M$, $0.2M$, $0.1M$, $0.05M$ and $0.025M$ respectively were $0.546$, $0.314$, $0.159$, $0.089$ and $0.071$. The data has a positive linear trend. For the unknown, we got $0.256$ for its absorbance. Based on the linear equation found from the 5 points above, we concluded the unknown's concentration to be $0.170M$. During the lab, possible errors could be that I put a bit too much solution in, or failed to adjust the spectrophotometer,...