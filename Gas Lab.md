# Gas Lab

## Part 1

**Formula** $ Ca + 2HNO_3 \rightarrow Ca(NO_3)_2 + H_2 $

### Experimental data

|                                         | Trial 1  | Trial 2 |
| --------------------------------------- | -------- | ------- |
| Mass of $ Ca \ (g) $                    | $0.044$  | $0.040$ |
| Room Temperature $(K)$                  | $293$    | $292.5$ |
| $ VP_{H_2O} \ (atm)$                    | $0.025$  | $0.026$ |
| $ P_{room} \ (atm) $                    | $1.0105$ | $1.005$ |
| $ P_{H_2} \ (atm) $                     | $0.985$  | $0.979$ |
| Observed volume of $ H_2 $ gas $ (ml) $ | $19.5$   | $21.5$  |

### Theoretical data

$$
m_{Ca} = n_{H_2} \times 40.078
\\
n_{H_2} = \frac{P_{H_2} \times V_{H_2}}{R \times T_{room}}
\\
V_{H_{2}} = 25ml = 0.025l
\\
\begin{align}
\\
Trial \ 1 && Trial \ 2
\\
n_{H_2} = \frac{0.985 \times 0.025}{0.0821 \times 293} = 0.001024 && n_{H_2} = \frac{0.979 \times 0.025}{0.0821 \times 292.5} = 0.001019 
\\
m_{Ca} = 0.001024 \times 40.08 = 0.0410g && m_{Ca} = 0.001019 \times 40.08 = 0.0408g
\end{align}
$$

### Error

$$
\%_{error} [1]= |\frac{19.5-25.0}{25.0}| \times 100\% = 22\%
\\
\%_{error} [2]= |\frac{21.5-25.0}{25.0}| \times 100\% = 14\%
\\
\overline{\%_{error}} = \frac{22\% + 14\%}{2} = 18\%
$$







## Part 2

**Formula** $ Mg + 2HCl \rightarrow H_2 + MgCl_2 $

### Experimental data

|                                         | Trial 1  | Trial 2  |
| --------------------------------------- | -------- | -------- |
| Mass of $ Mg \ (g) $                    | $0.025$  | $0.026$  |
| Room Temperature $(K)$                  | $293$    | $293$    |
| $ VP_{H_2O} \ (atm)$                    | $0.026$  | $0.025$  |
| $ P_{room} \ (atm) $                    | $1.0157$ | $1.0101$ |
| $ P_{H_2} \ (atm) $                     | $0.9897$ | $0.9851$ |
| Observed volume of $ H_2 $ gas $ (ml) $ | $25.5$   | $26.5$   |

### Theoretical data

$$
n_{H_2} = \frac{m_{Mg}}{M_{Mg}} = \frac{0.025 \times 1000}{24.30} = 0.00103
\\
V_{H_2} = \frac{n_{H_2} \times R \times T_{room}}{P_{H_2}} 
\\
\begin{align}
\\
Trial \ 1
\\
V_{H_2} &= \frac{0.00103 \times 0.0821 \times 293}{0.9897} \\
&= 0.0250l = 25.0ml
&&
\\\\
Trial \ 2
\\
V_{H_2} &= \frac{0.00103 \times 0.0821 \times 293}{0.9851} \\
&= 0.0248l = 24.8ml
\end{align}
$$

### Error

$$
\%_{error} [1]= |\frac{25.5 - 25.0}{25.0}| \times 100\% = 2\%
\\
\%_{error} [2]= |\frac{24.8 - 26.5}{26.5}| \times 100\% = 6\%
\\
\overline{\%_{error}} = \frac{2\% + 6\%}{2} = 4\%
$$

## Conclusion

​	The purposes of this lab were to find the theoretical, actual yield of hydrogen gas and the actual mass of metal in a reaction, then evaluate the results using stoichiometry, ideal gas law and error analysis. For the hydrogen part, the empirical formula was $ Ca + 2HNO_3 \rightarrow Ca(NO_3)_2 + H_2 $. 

​	For part 1, the two experimental attempts yielded $19.5ml \ H_2$ and $21.5ml \ H_2$ for two attempts, respectively. The theoretical yield was $25ml \ H_2$. This resulted in a $18\%$ error. During the experiment, our sample size for $Ca$ was $0.044g$ and $0.040g$. Theoretically, we should be using $0.0410g$ and $0.0408g$. I believe this is the largest cause of influence on the data and the error. Another cause would be the atmospheric conditions, such as temperature or pressure differing from day to day.

​	For part 2, the two experimental attempts yielded $25.5ml \ H_2$ and $26.5ml \ H_2$ for two attempts, respectively. The theoretical yields were $25.0ml \ H_2$ and $24.8ml \ H_2$ for two attempts, rspectively. After calculations shown in the section above, attempt 1 returned $2\%$ error, while attempt 2 returned $6\%$. An averaged out percentage is $4\%$. The most obvious cause for the high error percentage in attempt 2 would be using $0.026g$ of $Mg$ instead of the theoretical $0.025$. Similar to part 1, atmospheric conditions such as temperature or pressure differing from day to day would also impact the results and make it less accurate. 
